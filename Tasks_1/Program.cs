﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Tasks_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            
            //displaying the thread id
            //I am doing this so that I can confirm that when that task starts, it is operating on a separate thread
            Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);

            //here is a simple task with the lambda for a delegate
            //creating a task and then start the task right away
            Task t = Task.Run(()=>
            {
                for(int i=0;i<10;i++)
                {
                    Console.WriteLine("value of {0}", i);
                    //displaying the thread id
                    Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                }
            }
            );

            //waiting till the Task t is completed
            t.Wait();

            Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);

            //here is another thrdead, that returns a value when it is done
            Task<int> t2 = Task.Run(() =>
            {
                int hello = 0;
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("value of {0}", i);
                    //displaying the thread id
                    Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                    hello = hello + 1;
                }

                return (hello);
            }
            );

            t2.Wait();

            Console.WriteLine("Task result is {0} - before continuation", t2.Result);

            //now, lets try a task continuation

            t2.ContinueWith((x) =>
            {
                return (x);
            });

            Console.WriteLine("Task result is {0} - after continuation", t2.Result);

            Console.WriteLine("Current thread is {0} - after continuation", Thread.CurrentThread.ManagedThreadId);

            //now lets announce the thread completion

            var task_completed = t2.ContinueWith((x) =>
            {
                Console.WriteLine("task has been completed");
                Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
            }, TaskContinuationOptions.OnlyOnRanToCompletion);  //this onlyonrantocompletion will be reached only when task is completed

            //wait for this to be completed
            task_completed.Wait();
            Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
            Console.ReadLine();

            //lets try some tasks with children

            //this will be the parent task
            Task parent_child = Task.Run(()=>
            {
                Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                //now creating some new tasks as this parent's task with some childs

                //child task 1
                new Task( () =>
                    {
                        for(int i=0;i<10;i++)
                        {
                            Console.WriteLine(" Child task 1 value of {0}", i);
                            Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                        }
                    },TaskCreationOptions.AttachedToParent
                    ).Start();

                //child task 2
                new Task(() =>
                {
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine(" Child task 2 value of {0}", i);
                        Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                    }
                }, TaskCreationOptions.AttachedToParent
                    ).Start();

                //child task 3
                new Task(() =>
                {
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine(" Child task 3 value of {0}", i);
                        Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                    }
                }, TaskCreationOptions.AttachedToParent
                    ).Start();

                Console.ReadKey();
            }
                );



            var last_task = parent_child.ContinueWith(
                temp =>
                {
                    Console.WriteLine("This is the parent task");
                    Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);
                }
                );

            //now waiting for all children to finish
            //last task contains a reference to parent_child
            last_task.Wait();
            Console.WriteLine("Current thread is {0}", Thread.CurrentThread.ManagedThreadId);

            Console.ReadLine();
        }
    }
}
